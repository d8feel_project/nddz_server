/**
 * Created by Mic on 16/2/23.
 */
module.exports = {
    Room: {   //游戏场 id -> 普通场|比赛场
        Normal: {
            Room1: {
                name: "normal_room1",
                id: 1,
                baseBet: 50,    //底分
                money: 1000,    //最低金币要求
                ticket: 250,    //对局门票
                serverId: 'room-server-1'
            },
            Room2: {
                name: "normal_room2",
                id: 2,
                baseBet: 100,
                money: 10000,
                ticket: 800,
                serverId: 'room-server-2'
            },
            Room3: {
                name: "normal_room3",
                id: 3,
                baseBet: 1000,
                money: 100000,
                ticket: 1600,
                serverId: 'room-server-3'
            }
        },
        Rank: {}
    },
    Desk: {
        Status: {
            NotFull: 0,
            Full: 1,
            AllReady: 2,
            Gaming: 3,
            NoReady:4  //没有一个人准备
        },
        Code: {
            No_spring: 1,        // no spring
            Dizhu_spring: 2,     //spring dizhu win
            Farmer_spring: 3,    //spring  farmer win
            Dizhu_win: 4,
            Farmer_win: 5
        },
        Time: {
            Jiao: 15,
            Qiang: 15,
            Double:10,
            Play: 20
        },
        Record:{
            Jiao:1,
            UnJiao:2,
            Qiang:3,
            UnQiang:4,
            Double:true,
            UnDouble:false
        }
    },
    Player: {
        Status: {
            Room_Money_No: 0,    //choose rooms not enough money
            Desk_Money_No: 1,    //enter desk not enough money
            Disconnect: 2,       //掉线
            Hosting: 3,          //托管
            Normal: 4            //正常
        },
        Money: 10000               //领取救济金的数目
    },
    Code: {
        Fail: 500,
        Ok: 200
    },
    Event: {
        FirstLogin: 'onFirst',
        GetHelpMoney: 'onhelpMoney',
        LoginAndHelp: 'onLogin&Help',
        CannotGetHelp:'onGotoShop'      //领取次数达到上限
    }
};