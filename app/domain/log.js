/**
 * Author  Mic
 * Date 16/5/24
 */

/**
 * Created by Mic on 16/3/14.
 */

var fs = require('fs');
var util = require('util');

var log_path = 'test.log';

var log = function () {

};
var handler = log.prototype;

handler.log = function (str) {
    fs.exists(log_path, function (ok) {

        if (ok) {
            fs.appendFile(log_path, '\n{' + str + '}\n', function (err) {
                if (err) {
                    console.error(err);
                }
            });
        } else {
            console.error(str);
            fs.mkdir(log_path);

        }
    });
};
handler.warn = function (str) {
    fs.exists(log_path, function (ok) {
        if (ok) {
            fs.appendFile(log_path, str + '\n', function (err) {
                if (err) {
                    console.error(err);
                }
            });
        } else {

        }
    });
};
handler.fatal = function (str) {
    fs.exists(log_path, function (ok) {
        if (ok) {
            fs.appendFile(log_path, str + '\n', function (err) {
                if (err) {
                    console.error(err);
                }
            });
        }
    });
};

module.exports = new log();


