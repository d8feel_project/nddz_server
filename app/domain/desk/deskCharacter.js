/**
 * Created by Mic on 16/2/23.
 */
var pomelo = require('pomelo');
var consts = require('../../consts/consts');
var PlayerPool = require('./../player/playerPool');
var channelUtil = require('../../util/channelUtil');
var logger = require('pomelo-logger').getLogger(__filename);
var _ = require('underscore');
var node_uuid = require('node-uuid');
var RobotInfo = require("../../../config/hundredRobot.json");

var DeskCharacter = function (opts) {
    this.serverId = opts.serverId;
    this.name = opts.name;
    this.roomName = opts.roomName;
    this.deskId = opts.id;
    this.baseBet = opts.baseBet;
    this.money = opts.money;        //最低金币要求
    this.ticket = opts.ticket;      //对局门票
    this.robot = opts.robot;

    this.MAX_MEMBER_NUM = 3;
    this.channel = null;
    this.status = consts.Desk.Status.NotFull;

    this.playerList = [];
    this.playerList.length = this.MAX_MEMBER_NUM;
    this.beishu = 1;
    this.dipai = new Array(this.MAX_MEMBER_NUM);
    this._ondesk = [];
    this.timingId = null;           //计时器
    this.playerPool = null;         //玩家池
    this.curTalkIndex = -1;
    this.qiangResultArr = [];      //叫地主和抢地主的结果
    this.dizhuMark = -1;           //地主的标记 不是真正的地主
    this.noPointCount = 0;         //不叫地主或者不抢地主的个数 for judge the dizhu
    this.qiangCount = 0;           //抢或者不抢的次数
    this._remainsCards = [];       //剩余所有的牌
    this._pass = 0;                //过的次数
    this.playerKind = new Array(this.MAX_MEMBER_NUM);          //玩家的种类 正常玩家为0  机器人为1
    this._start = false;
    this.dizhu = -1;

    //存储出的牌
    this.playValue = [];    //玩家的操作
    this.playIndex = [];    //对应的玩家的位置

    this.recordTimer = null;
    this.playTime = 0;
    this.playRoute = {      //记录发送需要玩家操作的广播
        route: null,
        seatIndex: null,
        time: null
    };

    var _this = this;
    var init = function () {
        _this.createChannel();
        _this.playerPool = new PlayerPool();
    };
    init();
};
var handler = DeskCharacter.prototype;

//channel
handler.createChannel = function () {
    if (!this.channel) {
        var channelName = channelUtil.getRoomChannelName(this.name);
        this.channel = pomelo.app.get('channelService').getChannel(channelName, true);
    }
    return this.channel;
};
handler.addPlayer2Channel = function (playerInfo) {
    if (!this.channel) {
        return false;
    }
    if (playerInfo) {
        this.channel.add(playerInfo.uuid, playerInfo.sid);
        return true;
    }
    return false;
};
handler.removePlayerFromChannel = function (playerInfo) {
    if (!this.channel) {
        return false;
    }
    if (playerInfo && this.channel.getMember(playerInfo.uuid)) {
        this.channel.leave(playerInfo.uuid, playerInfo.sid);
        return true;
    }
    return false;
};
handler.pushMsg2All = function (route, content) {
    if (!this.channel) {
        return false;
    }
    var msg = {route: route, data: content};
    this.channel.pushMessage(msg);
    return true;
};
//player  value -> 明牌 和 正常开始
handler.enterDesk = function (playerInfo, value) {
    var money = playerInfo.money;
    if (this.checkPlayerMoney(money)) {
        this.addPlayer(playerInfo, value);

        return true;
    } else {
        logger.warn('玩家钱不够,不能再这里玩', money + ' < ' + this.money);
        return false;
    }
};
handler.addPlayer = function (playerInfo, value) {
    var seatIndex = this.getSeatIndex();
    playerInfo.deskName = this.name;
    playerInfo.roomName = this.roomName;

    this.playerList[seatIndex] = this.playerPool.addPlayer(seatIndex, playerInfo, value, this);
    this.playerKind[seatIndex] = 0;
    this.addPlayer2Channel(playerInfo);
    this.updateDeskInfo();
};
handler.addRobot = function () {
    var self = this;
    var seatIndex = this.getSeatIndex();
    if (seatIndex == -1)return;

    var nameId = parseInt(Math.random() * 50);
    var robotInfo = RobotInfo[nameId];
    var money = robotInfo.money;
    var url = robotInfo.avatar;
    var uuid = robotInfo.uid;
    var name = robotInfo.name;
    var info = {
        headUrl: url,
        uuid: uuid,
        userName: null,
        deskName: this.name,
        roomName: this.roomName,
        money: money
    };
    info.userName = name;
    self.playerKind[seatIndex] = 1;
    self.playerList[seatIndex] = self.playerPool.addRobot(seatIndex, info, self);
    console.log('添加机器人成功');
    self.updateDeskInfo();
};

handler.getSeatIndex = function () {
    for (var i = 0; i < this.playerList.length; i++) {
        var pi = this.playerList[i];
        if (pi === undefined || pi === null) {
            return i;
        }
    }
    return -1;
};
handler.setStatus = function (status) {
    this.status = status;
};
handler.getStatus = function () {
    return this.status;
};

handler.checkPlayerMoney = function (money) {
    if (money < this.money) {
        return false;
    }
    return true;
};

handler.getPlayerBySeatIndex = function (seatIndex) {
    var uuid = this.playerList[seatIndex].uuid;

    return this.playerPool.getPlayerByUuid(uuid);
};

handler.reset = function () {
    this.beishu = 1;
    this.dipai = new Array(this.MAX_MEMBER_NUM);
    this._ondesk = [];
    this.curTalkIndex = -1;
    this.qiangResultArr = [];      //叫地主和抢地主的结果
    this.dizhuMark = -1;           //地主的标记 不是真正的地主
    this.noPointCount = 0;         //不叫地主或者不抢地主的个数 for judge the dizhu
    this.qiangCount = 0;           //抢或者不抢的次数  //或者叫地主的个数 决定开始出牌
    this._remainsCards = [];       //剩余所有的牌
    this._pass = 0;
    this.playerKind = new Array(this.MAX_MEMBER_NUM);
    this._start = false;
    this.dizhu = -1;

    this.playValue = [];    //玩家的操作
    this.playIndex = [];    //对应的玩家的位置

    this.recordTimer = null;
    this.playTime = 0;
    this.playRoute = {      //记录发送需要玩家操作的广播
        route: null,
        seatIndex: null,
        time: null
    };

    this.playerPool.resetPlayer();
};

handler.setBeishu = function (value) {
    this.beishu *= value;
};
handler.getBeishu = function () {
    return this.beishu;
};


module.exports = DeskCharacter;