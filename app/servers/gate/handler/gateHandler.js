var dispatcher = require('../../../util/dispatcher');
var pomelo = require('pomelo');

var channelService = pomelo.app.get('channelService');

module.exports = function(app) {
    return new Handler(app);
};

var Handler = function(app) {
    this.app = app;
};

var handler = Handler.prototype;

handler.queryEntry = function(msg, session, next) {
    //var uid = msg.uuid;
    var uid = msg.uid;
    console.log('\n\n',msg);
    console.log("*****************start*******************");
    if (!uid) {
        next(null, {
            code: 500
        });
        return;
    }
    // get all connectors
    var connectors = this.app.getServersByType('connector');
    if (!connectors || connectors.length === 0) {
        next(null, {
            code: 500
        });
        return ;
    }
    var res = dispatcher.dispatch(uid, connectors);
    next(null, {
        code: 200,
        host: '127.0.0.1',
        //host:"192.168.199.212",
        //host:"192.168.1.100",
        //host:'120.24.246.144',
        // host:"114.55.92.111",
        port: res.clientPort
    });
};