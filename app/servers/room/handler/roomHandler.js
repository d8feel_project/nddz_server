/**
 * Created by Mic on 16/2/22.
 */

var pomelo = require('pomelo');

var handler = module.exports;

handler.enterDesk = function (msg, session, next) {
    var sid = session.get('sid');
    pomelo.app.rpc.room.roomRemote.enterDesk(session, msg, sid, next);
};

handler.exitRoom = function (msg, session, next) {
    var sid = session.get('sid');
    pomelo.app.rpc.room.roomRemote.exitRoom(session, msg, sid, next);
};
handler.test = function(msg,session,next){

};