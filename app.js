var pomelo = require('pomelo');
var routeUtil = require('./app/util/routeUtil');
var Room = require('./app/domain/room');

/**
 * Init app for client.
 */
var app = pomelo.createApp();
app.set('name', 'ddz-websocket');

// app configuration
app.configure('production|development', 'connector', function(){
	app.set('connectorConfig',
		{
			connector : pomelo.connectors.hybridconnector,
			heartbeat : 3,
			useDict : true,
			useProtobuf : true
		});
});

app.configure('production|development', 'gate', function(){
	app.set('connectorConfig',
		{
			connector : pomelo.connectors.hybridconnector,
			useProtobuf : true
		});
});

app.configure('production|development',function(){

	app.loadConfig('memcached',app.getBase()+'/config/memcached.json');
	var memclient = require('./app/dao/memcached/memcached').init(app);
	app.set('memclient',memclient);
	//add mysql
	app.loadConfig('mysql',app.getBase()+'/config/mysql.json');

});

app.configure('production|development', function() {
	var dbclient = require('./app/dao/mysql/mysql').init(app);
	app.set('dbclient', dbclient);
});

// app configure
app.configure('production|development', function() {
	// route configures
	//app.route('chat', routeUtil.chat);
	app.route('room',routeUtil.room);
	// filter configures
	app.filter(pomelo.timeout());
});

app.configure('production|development','room',function(){

	var server = app.curServer;
	var roomId = server.roomId;
	app.room =  new Room(require('./config/room/room.json')[roomId]);
});

// start app
app.start();

process.on('uncaughtException', function(err) {
	console.error(' Caught exception: ' + err.stack);
});